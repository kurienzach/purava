$(document).ready(function() {
    resize_sidebar();
    get_weather_data();
    render_scene(1);

    // Testimonial slider loop
    setInterval(function(){
      testimonial_slider();   
    },9000);
});

$(window).resize(function() {
    resize_sidebar();
    scene_resize();
    modal_resize();
});

function resize_sidebar () {
    // Hide testimonial if sidebar list is long
    if ($sidebar_list.offset()["top"] + $sidebar_list.height() > $testimonial.offset()["top"]) {
        $testimonial.addClass("scene-hide");
    } else {
        $testimonial.removeClass("scene-hide");
    }
}

function modal_resize() {
    if (!modal_visible)
        return;

    var $table = $modal_container.find(".table");

    // if there is a table inside the modal
    if ($table.length > 0) {
        var elems_height = 0;
        var table_row_height = 50;
        var modal_padding = 40;
        var $modal_content = $modal_container.find(".modal-content > div > div");

        // Height and width inclusive of the padding
        var modal_height = $modal_content.parent().parent().height(); 
        var modal_width = $modal_content.parent().parent().width();

        // Find the heights of all elements other than the table 
        $.each($modal_content.children().not(".table"), function(index, elem) {
            elems_height += $(elem).outerHeight(true);
        });

        // Find the height of the table if only it was there
        var max_table_height = ($table.find(".scroll").children().length + 1) * table_row_height + 1;

        var table_height = (modal_height - elems_height) < max_table_height ? (modal_height - elems_height) : max_table_height;
        var table_top = $modal_content.find("p").first().position().top + $modal_content.find("p").first().outerHeight(true);

        // Set the table top and table height
        $table.css("top", table_top);
        $table.outerHeight(table_height);

        $modal_content.find(".buttons").css("top", table_top + table_height);
        $modal_content.height(elems_height + table_height)

        $modal_content.parent().css("top", (modal_height - (elems_height + table_height)) / 2 + modal_padding);

        // Set the width of the last line of the table since percentage sizing goes wrong when 
        // scroll bars appear
        $table.find(".grand p").first().css("width", $table.find(".split").first().find("p:first-child").width());
        $table.find(".grand p").last().css("width", $table.find(".split").first().find("p:last-child").width());
    }
}

function testimonial_slider () {
  // Find the current shown testimonial
  var $selected = $testimonial.find(".selected");
  var $next = $selected.next().length ? $selected.next() : $testimonial.find(".testimonial").first();
  $selected.velocity({"top": "-5%", "opacity": 0}, 500, function() {
    $selected.css({"top": "5%"});
    $selected.removeClass("selected");

    $next.velocity({"top": "0", "opacity": "1"}, 500, function() {
      $next.addClass("selected");
    })
  });
  
}

/***********************************************
        Event Handlers inside scene
************************************************/
// [TODO] Find a better place to put this and refactor
$(".close-btn").click(function() {
    var prev_scene = curr_scene_data["parent"];
    console.log("Clicked Close. Goto Scene " + prev_scene);

    if ("is_360" in curr_scene_data && curr_scene_data["is_360"]) {
        photosphere.remove();
        curr_scene_state.$scene_360.empty();
        curr_scene_state.$scene_360.unbind();
        curr_scene_state.$scene_360.addClass("scene-hide");
        curr_scene_state.$scene_360.parent().fadeOut();

        // Show instruction bar if it was not hidden before
        if (show_instruction_bar) {
            $(".instruction-info").fadeIn();
        }

        delete photosphere;
    }

    // Pop from the data string
    data_string.pop()  

    render_next_scene(prev_scene);
});

// Previous scene button uncollapse click event
$(".scene .prev-btn").click(function() {
  console.log("Clicked Back. UnCollapse");
  $curr_scene = curr_scene_state.$scene;
  $prev_scene = $(this).parent().parent().parent();

  if(curr_scene_state["id"] > 2) {
    scene_state[curr_scene_state["id"]-3].$scene.velocity({"translateX": "95%"});
  }

  if (($curr_scene.find(".top-bar")[0] && hasClass($curr_scene.find(".top-bar")[0], "scene-hide")) 
    || (!$curr_scene.find(".top-bar")[0])) {
    curr_scene_data = scenes_data[curr_scene_data["parent"]];
    curr_scene_state = scene_state[curr_scene_data["level"] - 1];
    // Pop from the data string
    data_string.pop()
  }
  else {
    curr_scene_data = scenes_data[scenes_data[curr_scene_data["parent"]]["parent"]];
    curr_scene_state = scene_state[curr_scene_data["level"] - 1]; 
    // Pop from the data string
    data_string.pop()
    data_string.pop()
  }

  $curr_scene.find(".bottom1-bar").addClass("scene-hide");
  $curr_scene.find(".right").addClass("scene-hide");
  $curr_scene.find(".top-bar").addClass("scene-hide");

  // [TODO] This is always hit code, put a check for this
  $prev_scene.find(".collapse .floor").removeClass("selected");
  $prev_scene.find(".collapse").addClass("scene-hide");

  // $curr_scene.find(".scene-content").addClass("scene-hide");



  $curr_scene.velocity({
      // "scaleX": "0.75",
      // "scaleY": "0.75",
      "opacity": 0.5
  }, 500);

  
  $prev_scene.velocity({"translateX": "0%"}, 
      500, function() {
            scene_resize();
            $curr_scene.find(".img-holder").empty();

            // Show the weather info expect on the 3rd screen
            if (curr_scene_data["level"] == 1) {
                $(".bottom-bar").removeClass("scene-hide");
            }

            // Render the left bar menus again
            render_menu();
      });
});

// Virtual eye button click event
$("img.virtual-eye").click(function() {
    if (virtual_eye_visible == true) {
        show_virtual_eye(false);
    }
    else {
        show_virtual_eye(true);
    }
});


// Close instruction info on close button click
$(".instruction-info img").click(function() {
    show_instruction_bar = false;
    $(this).parent().fadeOut();
});

/***********************************************
                Modal events
************************************************/
// Modal events binding
$modal_container.click(function() {
    if (show_instruction_bar) {
        $(".instruction-info").fadeIn();
    }

    $(this).addClass("scene-hide");
    $(this).find(".modal-content").empty();
    modal_visible = false;
});

$modal_container.find(".modal").click(function (e) {
    e.stopPropagation();
})

$modal_container.find(".modal-close").click(function() {
    if (show_instruction_bar) {
        $(".instruction-info").fadeIn();
    }

    $modal_container.addClass("scene-hide");
    $modal_container.find(".modal-content").empty();
    modal_visible = false;
});

// Add click events to leftbar icons
$(".link-ico .fa-phone").click(function() {
    $modal_container.css("background", "rgba(0,0,0,0.4)");
    $modal_container.find(".modal").css("width", "78%");
    show_modal("#contact-modal");
});
$(".link-ico .fa-file").click(function() {
    window.open('brochure.pdf');
});
$(".link-ico .fa-map-marker").click(function() {
    $modal_container.css("background", "rgba(0,0,0,0.4)");
    $modal_container.find(".modal").css("width", "78%");
    show_modal("#info-modal");
    $(".info-modal .map-img img").loupe();
});
$(".link-ico .fa-camera").click(function() {
    $modal_container.css("background", "rgba(0,0,0,0.4)");
    $modal_container.find(".modal").css({"width": "78%"});
    $modal_container.find(".modal-close").hide();
    show_modal("#video-modal");
});

// Show modal on click of book now button
$("#scene3 button").click(function() {
    $modal_container.find(".modal").css("width", "74%");
    show_modal("#user-modal");
});
