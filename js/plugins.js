// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

// String left padding plugin
String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

// Show hide loader
function loader_hide(value, hide) {
    $loader = $(".loader");
    if (value) {
        // $loader.addClass("scene-hide");
        $loader.velocity({"opacity": 0}, 500, function() {
          if (hide)
              $loader.css("z-index", 0);  
        });
    } else {
        $loader.css("z-index", 900);
        $loader.velocity({"opacity": 0.7}, 500);
        // $loader.removeClass("scene-hide");
    }
}

// Helper to rotate compass
function rotate_compass (degree) {
    curr_scene_state.$scene.find(".compass").css("transform",
        "perspective(250px) rotateX(30deg) rotateZ(" + degree + "deg)");
}

// Query and display weather data
function get_weather_data () {
    $.getJSON("http://api.openweathermap.org/data/2.5/weather?lat=12.983&lon=77.530", function(data){
        $weather = $(".bottom-bar .weather");
        $weather.find("h2 strong").html(Math.floor(data["main"]["temp"] - 273.15));
        $weather.find("p").html(data["weather"][0]["description"]);

        // Weather icon
        console.log("Weather", data);
        switch(Math.floor(data["weather"][0]["id"] / 100)) {
          case 2:
              $weather.find("i").addClass("wi-thunderstorm");
              break;
          case 3:
              $weather.find("i").addClass("wi-rain-mix");
              break;
          case 5:
              $weather.find("i").addClass("wi-rain");
              break;
          case 6:
              $weather.find("i").addClass("wi-snow");
              break;
          case 7:
              $weather.find("i").addClass("wi-smoke");
              break;
          case 8:
              $weather.find("i").addClass("wi-day-cloudy");
              break;
          case 8:
              $weather.find("i").addClass("wi-day-cloudy-windy");
              break;
        }
    });
}

function reset_scene_data (obj) {
    obj.scene_loaded = 0;
    obj.scene_history = [];
}

// [TODO] the hover overlay is unecessarily removed, need not remove it actually
function disable_hover (value) {
    if(value) {
        curr_scene_state.$scene_img_holder.find(".hover-overlay svg").remove();
    } else {
        curr_scene_state.$scene_img_holder.find(".hover-overlay").show();
    }
}

function collapse_scene(id) {
    $scene = $("#scene" + id);
    // $scene.css({left: 100 - id * 4 + "%"});
    $scene.transition({"x": 100 - id * 4 + "%"}, 1000);
    render_scene(3);
}

function uncollapse_scene(id) {
    $scene = $("#scene" + id);
    // $scene.css({left: 0});
    $scene.transition({"x": "0%"}, 1000);
}

function show_tooltip (show, coordinates, text) {
    if (show) {
        if (!timeoutId) {
            timeoutId = window.setTimeout(function() {
                timeoutId = null; // EDIT: added this line
                $tooltip.find("p").html(text);

                $tooltip.css({
                    "left": (curr_scene_state.$scene_img_holder.position().left + coordinates.left * scaleFactor + $tooltip.find(".pointer").width() / 2) + "px",
                    "top": (curr_scene_state.$scene_img_holder.position().top + coordinates.top * scaleFactor - $tooltip.height() / 2) + "px"
                });
                
                $tooltip.show();
            }, 250);
        }
    }
    else {
        if (timeoutId) {
            window.clearTimeout(timeoutId);
            timeoutId = null;
        }
        else
            $tooltip.hide();
    }
}

// Helper functions to check if class applied to element
function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}


// Helper to show modal window with compiled template
function show_modal (template_id, options) {
    if (!modal_visible) {
      modal_visible = true;
      var template = _.template($(template_id).html());
      $modal_container.find(".modal-content").empty().html(template(options));
      $modal_container.removeClass("scene-hide");
      modal_resize();
      $(".instruction-info").fadeOut();
    }
    else {
        var template = _.template($(template_id).html());
        $modal_container.find(".modal-content").empty().html(template(options));
        modal_resize();
    }

    // Assign click events based on modal
    var flat_data = scenes_data[data_string[data_string.length-2] + "00"][data_string[data_string.length-1]];
    if (template_id === "#user-modal") {
      console.log("user");
      $modal_container.find(".contact-form button").click(function () {
        show_modal("#price-modal", {
            base_price: inrFormat(flat_data["basic_rate"]),
            area: flat_data["area"],
            total_basic: inrFormat(flat_data["total_basic"]),
            category: flat_data["category"],
            category_charges: inrFormat(flat_data["category_charges"]),
            floor_rise_charge: inrFormat(flat_data["floor_rise_charge"]),
            club_house_charges: inrFormat(flat_data["club_house_charges"]),
            other_charges: inrFormat(flat_data["other_charges"]),
            car_charges: inrFormat(flat_data["car_park"]),
            total_cost: inrFormat(flat_data["grand_total"]),
            type: flat_data["type"],
            flat_no: data_string[data_string.length-1],
        });
      });
    }
    else if (template_id === "#price-modal") {
      $modal_container.find(".price-details .payment").click(function() {
        show_modal("#payment-modal", {
            type: flat_data["type"],
            flat_no: data_string[data_string.length-1],
        });
      });   
    }
    else if (template_id === "#payment-modal") {
      $modal_container.find(".payment-plan .payment").click(function() {
        show_modal("#price-modal", {
            base_price: inrFormat(flat_data["basic_rate"]),
            area: flat_data["area"],
            total_basic: inrFormat(flat_data["total_basic"]),
            category: flat_data["category"],
            category_charges: inrFormat(flat_data["category_charges"]),
            floor_rise_charge: inrFormat(flat_data["floor_rise_charge"]),
            club_house_charges: inrFormat(flat_data["club_house_charges"]),
            other_charges: inrFormat(flat_data["other_charges"]),
            car_charges: inrFormat(flat_data["car_park"]),
            total_cost: inrFormat(flat_data["grand_total"]),
            type: flat_data["type"],
            flat_no: data_string[data_string.length-1],
        });
      });
    }
    
}

// Functions to add commas to INR 
function inrFormat(nStr) { // nStr is the input string
     nStr += '';
     x = nStr.split('.');
     x1 = x[0];
     x2 = x.length > 1 ? '.' + x[1] : '';
     var rgx = /(\d+)(\d{3})/;
     var z = 0;
     var len = String(x1).length;
     var num = parseInt((len/2)-1);
 
      while (rgx.test(x1))
      {
        if(z > 0)
        {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        else
        {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
          rgx = /(\d+)(\d{2})/;
        }
        z++;
        num--;
        if(num == 0)
        {
          break;
        }
      }
     return x1 + x2;
 }